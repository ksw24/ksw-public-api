# KSW Public API

[[_TOC_]]

## Description
You can place orders for (some) of our products directly through a REST based API. This repository will explain this process.

As  is common with our automated production/order processes, you'll need to provide print ready PDF files for this process to work. Incoming jobs will be processed by a PDF-X/4 based preflighting system which will try to fix some issues, but dimensions etc. have to be right from the start on.

After placing an order with us, processing will start within a few minutes which offers a very narrow window for cancelling an order. Cancelling or updating an order is (at the moment) not possible via the API, too, so make sure to contact us as soon as possible if there are issues that need to be addressed.

## Setup
Before starting using our API, we'll need to create a user account for you in our ERP system and adjacent systems. For doing so, we'll need the following information:

* Full business name & address
* Vat ID
* Name and mail address of a contact person
* A mail address for PDF invoices

We'll also need to whitelist your IP(s) for the moment, so please provide any IP addresses that will send requests to our servers.

You can send all this information via mail to support@suesse-werbung.de, we'll take care of the rest for you.

In return, you'll receive a customer number for further use and a combination of username and password which is used for authenticating against the API gateway. Please keep all this information secret, as it could be used to create orders in your name. If this information should become leaked at any point, please inform us immediately, so we can lock the API access and create new credentials.

> __Note:__ If you work for a large company, there might be a need to split bills to different cost centers or local subsidies. This can be achieved by creating and passing different customer numbers. Please contact us for further details.

> __Note:__ Please also inform us about any changes in your company address, invoice mail address and anything else to ensure a smooth process. Thanks!

## Authentification

All requests to the API must be signed by using what is commonly refered to as `Basic Auth`, which is a combination of username and password encoded in Base64. You'll receive username and password after setup from us.

## Usage
After the setup process has been done, you can use our endpoints to place orders and query their status. The following routes are currently supported:

| Route | Method | Description |
| ---   | ---    | ---         |
| /v1/orders | POST | Endpoint for creating a new order |
| /v1/status | POST | Endpoint for receiving the current status of an order. |

The base URL will be supplied together with customer number and API credentials when setting everything up.

### /v1/orders

The `/v1/orders` endpoint accepts a JSON payload like the following:

```json
{
    "order": {
        "orderid": "987-654-321",
        "customerno": "123456",
        "distributor": "13",
        "responsemail": "feedback@yourcompany.com",
        "positions": [
            {
                "article": {
                    "artdesclang": "DE",
                    "articleno": 110411101,
                    "amount": 500,
                    "printfile": "https://lorempdf.com/140/85/1"
                },
                "shipping": {
                    "shipmentprovider": "DHL",
                    "shippingtype": "DHLPaket",
                    "receiver": {
                        "lastname": "Meier",
                        "street": "Holzmattenstraße 22",
                        "zip": 79336,
                        "city": "Herbolzheim",
                        "country": "DE"
                    }
                }
            }
        ]
    }
}
```

This payload is a minimum working example with all the mandatory fields set. Omitting any of these fields will result in the API returning an error and not creating an order.

For the time being, the shipping node can be extended with further information. A version with all currently supported fields could look like the following.

```json
"shipping": {
    "shipmentprovider": "DHL",
    "shippingtype": "DHLPaket",
    "deliverynote": "https://yourserver.com/deliverynotes/note.pdf",
    "receiver": {
        "company": "KSW",
        "firstname": "Florian",
        "lastname": "Meier",
        "addendum": "Team E-Business",
        "street": "Holzmattenstraße 22",
        "zip": "79336",
        "city": "Herbolzheim",
        "country": "DE",
        "phone": "+49(7643)801-0",
        "email": "florian.meier@ksw24.com"
    },
    "sender": {
        "company": "KSW",
        "firstname": "Florian",
        "lastname": "Meier",
        "addendum": "Team E-Business",
        "street": "Holzmattenstraße 22",
        "zip": "79336",
        "city": "Herbolzheim",
        "country": "DE",
        "phone": "+49(7643)801-0",
        "email": "florian.meier@ksw24.com"
    }
}
```

> __Note:__ When providing a delivery note, make sure it is provided as a single-paged PDF in DIN A4. Delivery notes will furthermore be printed in greyscale.

After sending a request with a payload like the one above, the API will return a descriptive message and further information. The HTTP status code will be set to `200` for successful requests or to `400` in case something went wrong.

| Status | Body | Description |
| ---   | ---    | ---         |
| 200 | ```{ "id" : orderUUID, "message" : "order has been created successfully." }``` | `orderUUID` will contain the UUID that is used to identify the order in our systems |
| 400 | ```{ "message" : "..." }``` | The returned object will contain a descriptive message in the `message` node. |

> __Note:__ Error handling will stop on the first found error and return the error message. Subsequent errorrs will not be processed/found and reported.

### /v1/status

The `/v1/status` endpoint accepts a JSON payload like the following:

```json
"order": {
    "orderid": "...",
    "customerno": "...",
}
```

> __Note:__ `orderid` is the UUID that was returned by the API beforehand, `customerno` is the one we have provided you when setting up.

After sending a request with a payload like the one above, the API will return one of two things:

* In case of a status `200`, the response will contain an array with two strings. The first item is a status-like value, see the table below. The second one is a more descriptive message in relation to the first one.
* In case of a status `400` the response will be a single message that gives further information on what went wrong.

| Status | Body | 
| ---   | ---    |
| 200 | ```{ "message" : ["...", "..."] }``` |
| 400 | ```{ "message" : "..." }``` |

Possible responses for a status `200` are

| [0] | [1] | 
| ---   | ---    |
| created | order has been created, waiting for next steps. |
| open | order is in open state, waiting for next steps. |
| prepress | prepress steps are running or order is waiting for impose. |
| printing | order is being printed or in postpress steps. |
| processing | order is being processed in our production. |
| shipped | _you will receive tracking numbers as an array in this status_ |



## Field definition

In the following, you'll find a field definition for the fields used in the data model. The column _Part of_ indicates which (sub)node this field can be found in.

Please refer to the code snippet above to see a simplified overview of the whole data structure.

| Field | Mandatory | Description | Part of |
| --- | :---: | --- | --- | 
| orderid | YES | The `orderid` is the primary identifier of your order in our systems. It acts as a linking element in our systems, bundling multiple line items (positions) in one order. | order | 
| customerno | YES | Your customer number needs to be transferred with every order to ensure that orders can be identified as your order. Different customer numbers might be used for systems that need different invoice receivers etc. | order | 
| distributor | YES | The `distributor` defines which address is used for mandatory information on the product. If you want your address present on this, please inform yourself about the responsibilites of the "Inverkehrbringer" and then contact us. | order | 
| responsemail | YES | Please provide a mail address which will be used for automated reports and other transactional emails. Make sure that "human-generated email" can be received under this address. | order | 
| artdesclang | YES | In case the ordered product needs mandatory information on it, this language will be used for the overlay files. According to laws, this should be in a language that is widely spoken in the receivers country, so possibly use the official language. Use ISO-3166 ALPHA-2 codes only, e.g. "DE" or "FR". Available languages will be provided by us. | article | 
| articleno | YES | Article number, a list of possible articles will be provided by us. | article | 
| amount | YES | The quantity to order. Please refer to our datasheets/article listings for further information on minimum and maximum order quantitiy. | article | 
| printfile | (YES) | If you want to order a printed product, this is a mandatory field. It should link to an artwork file according to our specifications for that product. `printfile` needs to be accessible via a simple GET operation, so provide authentication in the link, if needed. | article | 
| shipmentprovider | YES | The chosen shipment provider for this position. Please make sure that `shipmentprovider` and `shippingtype` is a valid combination. Possible combinations will be provided by us. | shipping | 
| shippingtype | YES | The chosen shipping type for this position. Please make sure that `shipmentprovider` and `shippingtype` is a valid combination. Possible combinations will be provided by us. | shipping | 
| deliverynote |  | If you want to pass a delivery note with your position, provide a link to that file here. Should be reachable via GET, provide authentication where needed in the link. Please note that delivery notes will only be printed in grayscale. | shipping | 
| | | _(The following fields also apply to the sender node)_ | |
| company |  | Company name of the receiver. | receiver | 
| firstname |  | First name of the receiver. | receiver | 
| lastname | YES | Family name of the receiver. At least this must be provided to ensure that a package can be delivered. | receiver | 
| addendum |  | This field can be used for additional information for the receiver. This might contain something like "marketing department", "4th floor" or a "Postnummer" when sending packages to DHL Packstationen. | receiver | 
| street | YES | Street name and number of the receiver. | receiver | 
| zip | YES | Zip code of the city. | receiver | 
| city | YES | Name of the city. | receiver | 
| country | YES | Country code in ISO-3166 ALPHA 2, e.g. "DE" or "FR". | receiver | 
| phone |  | Phone number of the receiver. Might be used by some shipping providers to inform the receiver of delivery dates or updates regarding shipment. | receiver | 
| email |  | Same as above, some shipment providers also/only send mails with information regarding the shipment. | receiver |  

## Shipping

For the moment, shipping is only possible via DHL, therefore only the following value pairs must be used for `shipmentprovider` and `shippingtype`.

| `shipmentprovider` | `shippingtype` | Costs | Description |
| ---   | ---    | ---    | ---         |
| DHL | DHLPaket | Varying | National and international shipping via DHL |

> __Note:__ We are open for your recommendations on which shipping provider and method we should integrate next.

We can inform you about shipping costs upfront and help you figuring out how to calculate the shipping cost for a given product, amount and country combination. Feel free to contact us for further help with this.

## Possible countries

> __Information:__ European regulation (EU No 1169/2011) requires customers to provide "mandatory food information [...] in a language easily understood by the consumers[...] where a food is marketed". Usually this translates to printing the mandatory information in the official language of the member state.

We provide templates with all mandatory information in a number of languages. We also recommend to use the following value-country-combinations for `artdesclang`. In countries where multiple official languages exist (eg. Switzerland), you are free to use any of the official languages.

| Shipping country | `artdesclang` | 
| ---   | ---    |
| DE / AT / CH | DE |
| DK | DK |
| ES | ES |
| FR / BE / LU | FR |
| GB / IE | GB |
| IT | IT |
| NL | NL |
| SE | SE |
| EE | EE |
| FI | FI |
| HU | HU |
| PL | PL |

## Support
The best way to contact us is through our support system, which can be reached via mail at support@suesse-werbung.de. Using that way will make sure that we get eyes and hands on any issues quickly.

Feel free to contact us with questions regarding the API or other automated processes even before starting to connect to those systems.

## Roadmap

Done:

- [x] POST /orders/ endpoint for creating new orders (05-2024)
- [x] POST /status/ endpoint for fetching status for existing orders (05-2024)
- [X] Release of v1 API (07-2024)

Backlog:

- [ ] Add more shipping providers and methods
- [ ] Your requirement? -> support@suesse-werbung.de

## Changelog

- 2024-07-15: added more information to the prepress status message